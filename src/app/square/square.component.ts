import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {
  boxes:any = {}
  selectedBox:any = {}
  keyboardControl:boolean = true;
  object = Object;

  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.addListner()
  }
  toogleKeyControl () {
    if (this.keyboardControl) {
      this.keyboardControl = false
      document.onkeydown = null;
    } 
    else {
      this.addListner()
      this.keyboardControl = true
    }
  }
  addListner () {
    let self = this
    document.onkeydown = function(e) {
      if(self.selectedBox && self.selectedBox.boxId) {
        switch (e.key) {
          case 'ArrowUp' :
          case 'w': 
          case 'W':
            self.selectedBox.top-=10;
            break;
          case 'ArrowDown': 
          case 'a':
          case 'A':
            self.selectedBox.top+=10;
            break;
          case 'ArrowLeft': 
          case 's':  
          case'S':
            self.selectedBox.left-=10;
            break;
          case 'ArrowRight':
          case 'd':
          case 'D':
            self.selectedBox.left+=10;
            break;
          case 'Delete':
            delete self.boxes[self.selectedBox.boxId]
            self.selectedBox = {}
            break
          default:
            break;
        }
        // let item:any = document.getElementById(self.selectedBox.boxId)
        // item.style.top =self.selectedBox.top+'px'
        // console.log(self.selectedBox, item)
      }
    };
  }
  ngOnDestroy () {
    document.onkeydown = null;
  }
  createBox () {
    const boxId = new Date().getTime()
    const allKeys = Object.keys(this.boxes)
    this.boxes[boxId] = {
      id:  allKeys.length ? this.boxes[allKeys[allKeys.length-1]].id+1 : 1,
      boxId: boxId,
      top: 0,
      left: 0,
      background: this.getDarkColor()
    }
  }
  getDarkColor() {
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += Math.floor(Math.random() * 10);
    }
    return color;
  }
  selectBox(box:any) {
    this.selectedBox = box
  }
}
